﻿Shader "PostProcessing/Blur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Focus("FocusDist", float) = 3.0
		_Strength("BlurStrength", float) = 1.0
		
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			float _Focus;
			int _Radius;
			float _Strength;
			sampler2D _CameraDepthTexture;
			

			fixed4 BoxBlur(sampler2D tex, sampler2D depth, float2 uv)
			{
				float cameraDepth = LinearEyeDepth(tex2D(_CameraDepthTexture, uv));
				float distance = saturate(abs(cameraDepth - _Focus) * .5f);
				//return fixed4(distance, 0, 0, 1);
				float2 delta = float2(1.0 / _ScreenParams.x, 1.0 / _ScreenParams.y);

				int radius = clamp(distance * + _Strength, 1, 5); //clamp(abs(floor(_Strength * (distance - _Focus))), 0, 5);

				//return fixed4(radius * 0.1f, 0, 0, 1);
				float count = 0;
				//return fixed4(radius * .5f, 0, 0, 1);
				

				fixed4 col = fixed4(0, 0, 0, 0);

				for (int i = -radius; i < radius; i++)
				{
					for (int j = -radius; j < radius; j++)
					{
						col += tex2D(tex, (uv + float2(i, j) * delta));
						count += 1;
					}
				}

				col = col/count;

				return fixed4(col.rgb, 1);
				//horiz += tex2D(tex, (uv + float2(-1, 0) * delta)) * 2.0;
				//return saturate(10 * sqrt(horiz * horiz + vert * vert));
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				// just invert the colors
				//col.rgb = 1 - col.rgb;
				fixed4 blur = BoxBlur(_MainTex, _CameraDepthTexture, i.uv);

				float cameraDepth = LinearEyeDepth(tex2D(_CameraDepthTexture, i.uv));
				float r = 0.05 *cameraDepth;
				//return fixed4(r,r,r,1);
				return blur;
			}
			ENDCG
		}
	}
}
