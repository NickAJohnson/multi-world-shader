﻿Shader "Custom/Dissolve"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType"="Transparent" }

		LOD 100

		ZWrite On
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			float4 _MainTex_ST;
			float _NoiseCutoff;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				if (_NoiseCutoff - .1f > tex2D(_NoiseTex, i.uv).r)
				{
					col.a = 0;
					clip(-1);
				}
				else if (_NoiseCutoff > tex2D(_NoiseTex, i.uv).r && col.a != 0)
				{
					col = fixed4(1,0,0,1);
				}

				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				//return fixed4(_NoiseCutoff * .1f, 0, 0, 1);
				return col;
			}
			ENDCG
		}
	}
}
