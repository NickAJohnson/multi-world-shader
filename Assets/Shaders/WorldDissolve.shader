﻿Shader "Custom/WorldDissolve"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Flip("Flip", Float) = 1.0
		_Count("Count", Int) = 0
	}	
	SubShader
	{
		Tags { "RenderType" = "Opaque" }

		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 worldPos : TANGENT;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Flip;
			float4 _Centres[10];
			float _RadiusArray[10];
			int _Count;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				o.worldPos = mul(UNITY_MATRIX_M, v.vertex);
				//o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				float flipValue = _Flip;
				for (int j = 0; j < _Count; j++)
				{
					flipValue *= distance(i.worldPos, _Centres[j]) - _RadiusArray[j];
					//col = fixed4((distance(i.worldPos, _Centres[_Count-1])-_RadiusArray[_Count-1]), 0, 0, 1);
				}

				clip(flipValue);

				// apply fog
				//UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG

			Cull Off
		}
	}
}
