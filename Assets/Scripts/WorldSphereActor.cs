﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSphereActor : MonoBehaviour {

    //Noise Texture and Material Properties
    public Material mat;
    private Texture2D noiseTex;

    public int pixWidth = 256;
    public int pixHeight = 256;

    public float scale = 20;

    private float offsetX = 10;
    private float offsetY = 10;

    //Sphere Properties
    public float spinSpeed = 1.0f;
    public float radius;
    

    //Other


    // Use this for initialization
    void Start ()
    {   
        //noise
        offsetX = Random.Range(0.0f, 99999.0f);
        offsetY = Random.Range(0.0f, 99999.0f);
        noiseTex = GenerateTexture();
        mat.SetTexture("_NoiseTex", noiseTex);

        radius = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(Vector3.up, spinSpeed * Time.deltaTime);
        radius += Time.deltaTime;
        if(radius < 100)
        {
            radius += ((100 - radius)/ 5) * Time.deltaTime;
        }
        Vector3 t = gameObject.transform.localScale;
        t = new Vector3(radius * 2, radius * 2, radius * 2);
        gameObject.transform.localScale = t;
    }

    #region NoiseTexture
    //generate noise texture
    private Texture2D GenerateTexture()
    {
        Texture2D tex = new Texture2D(pixWidth, pixHeight);

        for (int x = 0; x < pixWidth; x++)
        {
            for (int y = 0; y < pixHeight; y++)
            {
                Color color = CalculateColor(x, y);
                tex.SetPixel(x, y, color);
            }
        }
        tex.Apply();
        return tex;
    }

    //Calc value for noise texture
    private Color CalculateColor(int x, int y)
    {
        float xCoord = ((float)x + offsetX) / pixWidth * scale;
        float yCoord = ((float)y + offsetY) / pixHeight * scale;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(sample, sample, sample);
    }

    #endregion


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.gameObject.tag == "Tile")
        {
            TileActor tile = other.gameObject.GetComponent<TileActor>();
            if (tile != null)
            {
                tile.ToggleCollider();
            }
        }
    }

    public void setPosition(Vector3 newPos)
    {

    }
}
