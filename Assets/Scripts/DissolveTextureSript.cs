﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveTextureSript : MonoBehaviour {

    public Material mat;
    private Texture2D noiseTex;

    public int pixWidth = 256;
    public int pixHeight = 256;

    public float scale = 20;
    [Range(0.0f, 10.0f)]
    public float dissolveRate = 1.0f;

    private float offsetX = 10;
    private float offsetY = 10;

    [SerializeField]
    private float dissolveAmount = 0.0f;
	// Use this for initialization
	void Start ()
    {
        offsetX = Random.Range(0.0f, 99999.0f);
        offsetY = Random.Range(0.0f, 99999.0f);
        noiseTex = GenerateTexture();
        mat.SetTexture("_NoiseTex", noiseTex);
	}

    void Update()
    {
        dissolveAmount += Time.deltaTime * dissolveRate;
        mat.SetFloat("_NoiseCutoff", dissolveAmount);
        if(dissolveAmount > 1.0f)
        {
            dissolveAmount = 0;
            offsetX = Random.Range(0.0f, 99999.0f);
            offsetY = Random.Range(0.0f, 99999.0f);
            noiseTex = GenerateTexture();
            mat.SetTexture("_NoiseTex", noiseTex);
        }
    }

    private Texture2D GenerateTexture()
    {
        Texture2D tex = new Texture2D(pixWidth, pixHeight);

        for (int x = 0; x < pixWidth; x++)
        {
            for (int y = 0; y < pixHeight; y++)
            {
                Color color = CalculateColor(x, y);
                tex.SetPixel(x, y, color);
            }
        }
        tex.Apply();
        return tex;
    }

    private Color CalculateColor(int x, int y)
    {
        float xCoord = ((float)x + offsetX) / pixWidth * scale;
        float yCoord = ((float)y + offsetY) / pixHeight * scale;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(sample, sample, sample);
    }
}
