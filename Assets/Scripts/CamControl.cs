﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamControl : MonoBehaviour {

    public Transform target;
    public float speed = 90;
    public float distance = 0;
    public bool thirdPerson = true;
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;


	// Use this for initialization
	void Start ()
    {
	    
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {

        if (thirdPerson)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector3 angles = transform.eulerAngles;
            if (angles.x > 180)
            {
                angles.x -= 360;
            }
            angles.x = Mathf.Clamp(angles.x, -70, 70);
            angles.y += horizontal * Time.deltaTime * speed;

            transform.eulerAngles = angles;
            //transform.position = target.position - distance * transform.forward;
            transform.position += vertical * transform.forward;
        }
        else
        {
            this.transform.position = target.position + 1.8f * Vector3.up;
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
            smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1.0f / smoothing);
            smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1.0f / smoothing);
            mouseLook += smoothV;

            transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
            target.localRotation = Quaternion.AngleAxis(mouseLook.x, target.transform.up);
        }
    }
}
