﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraActor : MonoBehaviour {

    [SerializeField]
    private GameObject target;
    float distance;
    Vector3 offset;


	// Use this for initialization
	void Start ()
    {
        offset = this.transform.position - target.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = target.transform.position + offset;
	}
}
