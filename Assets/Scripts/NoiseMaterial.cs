﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaterial : MonoBehaviour
{

    public Material mat;
    private Texture2D noiseTex;

    public int pixWidth = 256;
    public int pixHeight = 256;

    public float scale = 20;
    // Use this for initialization
    void Start()
    {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = GenerateTexture();

    }

    // Update is called once per frame
    //void Update () {

    //}

    private Texture2D GenerateTexture()
    {
        Texture2D tex = new Texture2D(pixWidth, pixHeight);

        for (int x = 0; x < pixWidth; x++)
        {
            for (int y = 0; y < pixHeight; y++)
            {
                Color color = CalculateColor(x, y);
                tex.SetPixel(x, y, color);
            }
        }
        tex.Apply();

        return tex;
    }

    private Color CalculateColor(int x, int y)
    {
        float xCoord = (float)x / pixWidth * scale;
        float yCoord = (float)y / pixHeight * scale;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(sample, sample, sample);
    }
}
