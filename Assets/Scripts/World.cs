﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour {

    public Renderer[] renderers;
    public List<Material> materials;

    [SerializeField]
    public GameObject fireOrbPrefab;
    [SerializeField]
    private GameObject iceOrbPrefab;


    private List<WorldSphereActor> activeSpheres;

    public Vector4[] positions = new Vector4[10];
    public float[] radiuses = new float[10];

    public Transform playerPos;

    public int sphereCount = 0;
    // Use this for initialization

        //Initialise material and renderer arrays
    private void Awake()
    {
        positions = new Vector4[10];
        radiuses = new float[10];
        renderers = GetComponentsInChildren<Renderer>();
        for (int i = 0; i < renderers.Length; i++)
        {
            materials.Add(renderers[i].material);
            //materials[i].SetFloatArray("_Radiuses", radiuses);
        }
    }

    void Start () {
        
        activeSpheres = new List<WorldSphereActor>();
	}

	// Update is called once per frame
	void Update () {

        //Update Radius Array
        for(int i = 0; i < sphereCount; i++)
        {
            radiuses[i] = activeSpheres[i].radius;
        }
        foreach(Material m in materials)
        {
            m.SetFloatArray("_RadiusArray", radiuses);
        }
	}

    public void NewExplosion(Vector3 location, bool isIce)
    {
        if(sphereCount > 9)
        {
            return;
        }
        //create sphere in location
        GameObject newOrb;
        if(isIce)
        {
            newOrb = Instantiate(iceOrbPrefab, location, Quaternion.identity);
        }
        else
        {
            newOrb = Instantiate(fireOrbPrefab, location, Quaternion.identity);
        }

        //Add position and sphere to lists
        //positions[sphereCount] = new Vector4(newOrb.gameObject.transform.position.x, newOrb.gameObject.transform.position.y, newOrb.gameObject.transform.position.z, 1);
        positions[sphereCount] = new Vector4(location.x, location.y, location.z, 1);
        activeSpheres.Add(newOrb.GetComponent<WorldSphereActor>());

        sphereCount++;
        foreach (Material m in materials)
        {
            m.SetVectorArray("_Centres", positions);
            m.SetInt("_Count", sphereCount);
        }


        //Update Positions array in materials
        //TODO make spheres decrease in size after time or when another activated
    }
}
