﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileActor : MonoBehaviour {


    public BoxCollider m_collider;
	// Use this for initialization
	void Start ()
    {
        m_collider = GetComponent<BoxCollider>();	
	}

    public void ToggleCollider()
    {
        m_collider.enabled = !m_collider.enabled;
    }
	

}
