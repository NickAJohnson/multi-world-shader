﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbActor : MonoBehaviour {

    public bool isIce = false;

    [SerializeField]
    private World theWorld;

    // Use this for initialization
    void Start ()
    {
	    	
	}
	
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            theWorld.NewExplosion(this.transform.position, isIce);
            this.gameObject.SetActive(false);
        }
    }
}
